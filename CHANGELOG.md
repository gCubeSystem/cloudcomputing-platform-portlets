
# Changelog for Cloud Computing Platform Portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.1]

- read d4s-dynamic-scope configuration on Keycloak server from IS, and set uma = !d4s-dynamic-scope [#28129]
- added common/js/utils.js dependency
- moved IS configuration to Auth/IAM from Service/IAM

## [v1.0.0] - 2024-07-15

First Release
