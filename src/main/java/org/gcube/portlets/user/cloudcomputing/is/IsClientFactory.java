package org.gcube.portlets.user.cloudcomputing.is;

import com.liferay.portal.kernel.log.LogFactoryUtil;

// import org.gcube.common.security.AuthorizedTasks;
// import org.gcube.common.security.secrets.Secret;

/**
 * Utility class to query EndPoints and search for AccessPoints from IS
 * 
 * @author Alfredo Oliviero (ISTI - CNR)
 */

public class IsClientFactory {
    private static com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(IsClientFactory.class);
    static boolean cache_enabled = true;

    public static IsClient getSingleton(){
        return getSingleton(cache_enabled);
    }

    public static IsClient getSingleton(boolean use_cache){
        if (use_cache) {
            logger.info("@@@ IsClientFactory cache enabled");
            return CacheIsClient.getSingleton();
        } else {
            logger.info("@@@ IsClientFactory cache not enabled");

            return IsClient.getSingleton();
        }
    }
}
