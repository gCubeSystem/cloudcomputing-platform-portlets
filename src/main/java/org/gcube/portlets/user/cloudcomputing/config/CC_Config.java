package org.gcube.portlets.user.cloudcomputing.config;

public class CC_Config {

    public String encoded_context = ""; // "%2Fgcube%2Fdevsec%2FdevVRE";
    public String gateway = ""; // "next.dev.d4science.org";

    public String redirect_url = ""; // "https://next.dev.d4science.org/group/devvre/cloudcomputing";

    public String auth_url = ""; // "https://accounts.dev.d4science.org/auth";
    public String ccp_url = ""; // "https://ccp.cloud-dev.d4science.org";

    public String cdn_url = ""; // "https://cdn.cloud-dev.d4science.org";
    public boolean use_uma = true;
    public boolean d4s_dynamic_scope = false;
}
