package org.gcube.portlets.user.cloudcomputing;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.LogFactoryUtil;

public class CC_MethodEditorPortlet extends CC_Portlet {

    private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil
            .getLog(CC_MethodEditorPortlet.class);

    public void render(RenderRequest renderRequest, RenderResponse renderResponse)
            throws PortletException, IOException {
        super.render(renderRequest, renderResponse);
    }

}
