package org.gcube.portlets.user.cloudcomputing.is;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.rmi.ServerException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.gcube.common.portal.PortalContext;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.AccessPoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;

import com.liferay.portal.kernel.log.LogFactoryUtil;

// import org.gcube.common.security.AuthorizedTasks;
// import org.gcube.common.security.secrets.Secret;

/**
 * Utility class to query EndPoints and search for AccessPoints from IS
 * 
 * @author Alfredo Oliviero (ISTI - CNR)
 */

public class IsClient {
    private static com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(IsClient.class);

    private static IsClient _singleInstance = null;

    static IsClient getSingleton() {
        if (_singleInstance == null) {
            _singleInstance = new IsClient();
        }
        return _singleInstance;
    }

    /**
     * obatins from IS the list of ServiceEndpoint matching the parameters
     *
     * @param resource_name
     * @param category
     * @param root_service
     * @param secret
     * 
     * @return the list of EndPoints matching the parameters
     * @throws ServerException
     * @throws Exception
     */

    public ServiceEndpoint getFirstEndopintsFromIS(String resource_name, String category)
            throws ServerException {

        List<ServiceEndpoint> endpoints = getEndpointsFromIS(resource_name, category);
        if (endpoints == null || endpoints.size() == 0) {
            logger.error("Unable to retrieve service endpoint " + resource_name);
            return null;
        }

        return endpoints.get(0);

    }

    public List<ServiceEndpoint> getEndpointsFromIS(String resource_name, String category
) throws ServerException {
    logger.info("@@@ not cached getEndpointsFromIS: " + resource_name + ":" + category);

        SimpleQuery query = queryFor(ServiceEndpoint.class);

        if (resource_name != null) {
            query.addCondition("$resource/Profile/Name/text() eq '" + resource_name + "'");
        }
        if (category != null) {
            query.addCondition("$resource/Profile/Category/text() eq '" + category + "'");
        }
        DiscoveryClient<ServiceEndpoint> client = clientFor(ServiceEndpoint.class);

        List<ServiceEndpoint> endpoints = null;

        String currentScope = ScopeProvider.instance.get();
        String infrastructure = "/" + PortalContext.getConfiguration().getInfrastructureName();
        ScopeProvider.instance.set(infrastructure);

        try {
            logger.info (" IS CLIENT Requiring " + resource_name + ":"+ category);
            // if (root_service) {

            // endpoints = AuthorizedTasks.executeSafely(() -> {
            // // esegui la query
            // List<ServiceEndpoint> toReturn = client.submit(query);
            // return toReturn;
            // }, secret);
            // } else {
            endpoints = client.submit(query);
            // }
        } catch (Throwable e) {
            e.printStackTrace();
            throw new ServerException(e.getMessage());

        } finally {
            ScopeProvider.instance.set(currentScope);
        }

        return endpoints;

    }

    /**
     * obatains the list of AccessPoints matching the parameters
     *
     * @param resource_name
     * @param category
     * @param endPointName
     * @param is_root_service
     * @return the list of AccessPoints
     * @throws ServerException
     * @throws Exception
     */
    public List<ServiceEndpoint.AccessPoint> getAccessPointsFromIS(String resource_name, String category,
            String endPointName/* , boolean is_root_service, Secret secret */) throws ServerException {

        List<ServiceEndpoint> resources = getEndpointsFromIS(resource_name, category/* , is_root_service, secret */);

        if (resources.size() == 0) {
            logger.error("There is no Runtime Resource having name " + resource_name + " and Category "
                    + category + " in this scope.");
            return null;
        }

        List<ServiceEndpoint.AccessPoint> response = new ArrayList<ServiceEndpoint.AccessPoint>();
        resources.forEach(res -> {
            Stream<ServiceEndpoint.AccessPoint> access_points_res = res.profile().accessPoints().stream();

            if (endPointName == null) {
                access_points_res = access_points_res.filter(ap -> ap.name().equals(endPointName));
            }

            access_points_res.forEach(a -> response.add(a));
        });
        return response;
    }

    /**
     * obatains the list of AccessPoints matching the parameters, and returns the
     * first one
     * 
     * @param resource_name
     * @param category
     * @param entryPointName
     * @return an AccessPoints matching the parameters
     * @throws ServerException
     * @throws Exception
     */
    public ServiceEndpoint.AccessPoint getFirstAccessPointFromIS(String resource_name, String category,
            String entryPointName/* , boolean root_service, Secret secret */) throws ServerException {

        List<ServiceEndpoint.AccessPoint> access_points = getAccessPointsFromIS(resource_name, category,
                entryPointName /* , root_service, secret */);

        if (access_points == null || access_points.size() == 0) {
            logger.error("Unable to retrieve service endpoint " + entryPointName);
            return null;
        }

        for (AccessPoint ap : access_points) {
            if (ap.name().equals(entryPointName)) {
                return ap;
            }
        }

        logger.error("Unable to retrieve service endpoint " + entryPointName);
        return null;

    }

    /**
     * Reads the service configuration from the IS
     * 
     * @param resourceName
     * @param category
     * @param endPointName
     * @param is_root_service
     * @param secret
     * @return
     * @throws Exception
     */
    public IsServerConfig serviceConfigFromIS(String resourceName, String category, String endPointName
    /* , boolean is_root_service, Secret secret */)
            throws ServerException {

        logger.info("Starting creating service credentials");
        ServiceEndpoint.AccessPoint accessPoint = getFirstAccessPointFromIS(resourceName,
                category, endPointName/* , is_root_service, secret */);
        if (accessPoint == null) {
            String error_log = "Unable to retrieve service endpoint " + endPointName;

            logger.error(error_log);
            throw new ServerException(error_log);
        }

        try {
            IsServerConfig config = new IsServerConfig(accessPoint);
            return config;

        } catch (Exception e) {
            logger.error("cannot create server config from" + accessPoint, e);

            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }
}
