package org.gcube.portlets.user.cloudcomputing;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

public class CC_ConfigurationPortlet extends CC_Portlet {
    private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil
            .getLog(CC_ConfigurationPortlet.class);

    public void render(RenderRequest renderRequest, RenderResponse renderResponse)
            throws PortletException, IOException {

        String current_url = PortalUtil.getCurrentURL(renderRequest);
        renderRequest.setAttribute("current_url", current_url);

        String infrastructure = PortalContext.getConfiguration().getInfrastructureName();
        renderRequest.setAttribute("infrastructure", infrastructure);

        HttpServletRequest httpReq = PortalUtil
                .getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
        String absolute_current_url = PortalUtil.getAbsoluteURL(httpReq, current_url);
        renderRequest.setAttribute("absolute_current_url", absolute_current_url);

        String base_url = PortalUtil.getAbsoluteURL(httpReq, "/");
        renderRequest.setAttribute("base_url", base_url);

        try {
            URL burl = new URL(base_url);
            renderRequest.setAttribute("host", burl.getHost());

        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            renderRequest.setAttribute("host", e.getMessage());
        }

        try {
            Group group = (Group) GroupLocalServiceUtil.getGroup(PortalUtil.getScopeGroupId(renderRequest));
            String group_url = group.getFriendlyURL();
            renderRequest.setAttribute("group_url", group_url);
            renderRequest.setAttribute("group_name", group.getName());

        } catch (Exception e) {
            e.printStackTrace();
            renderRequest.setAttribute("group_url", e.getMessage());
            renderRequest.setAttribute("group_name", e.getMessage());
        }

        String completeURL = PortalUtil.getCurrentCompleteURL(httpReq);
        renderRequest.setAttribute("completeURL", completeURL);

        // User theUser = PortalUtil.getUser(renderRequest);
        String currentContext = getCurrentContext(renderRequest);
        renderRequest.setAttribute("current_context", currentContext);
        super.render(renderRequest, renderResponse);
    }
}
