package org.gcube.portlets.user.cloudcomputing.is;

import java.io.Serializable;
import java.util.List;

import org.gcube.common.resources.gcore.ServiceEndpoint;

public class CacheEntry implements Serializable {
    List<ServiceEndpoint> endpoints;
    long timestamp;

    CacheEntry(List<ServiceEndpoint> endpoints, long timestamp) {
        this.endpoints = endpoints;
        this.timestamp = timestamp;
    }
}
