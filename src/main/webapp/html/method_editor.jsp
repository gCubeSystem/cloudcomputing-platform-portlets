<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ include file="./init.jsp"%>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/ol@v9.2.4/dist/ol.js"></script>
<script src="${cdn_url}/ccp/js/inputwidgetcontroller.js"></script>
<script src="${cdn_url}/storage/d4s-storage.js"></script>
<script src="${cdn_url}/common/js/utils.js"></script>
<div>
    <d4s-ccp-methodeditor serviceurl="${ccp_url}">
        <script src="${cdn_url}/ccp/js/methodeditorcontroller.js"></script>
        <script src="${cdn_url}/ccp/js/inputwidgeteditorcontroller.js"></script>
        <script src="${cdn_url}/ccp/js/outputwidgeteditorcontroller.js"></script>
    </d4s-ccp-methodeditor>
</div>