<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ include file="./init.jsp"%>

<% 
pageContext.setAttribute("current_context", request.getAttribute("current_context"));

pageContext.setAttribute("current_url", request.getAttribute("current_url"));
pageContext.setAttribute("infrastructure", request.getAttribute("infrastructure"));
pageContext.setAttribute("absolute_current_url", request.getAttribute("absolute_current_url"));
pageContext.setAttribute("base_url", request.getAttribute("base_url"));
pageContext.setAttribute("group_url", request.getAttribute("group_url"));
pageContext.setAttribute("group_name", request.getAttribute("group_name"));
pageContext.setAttribute("host", request.getAttribute("host"));
%>

<h5> CCP configuration</h5>

<ul>
    <li> encoded_context = ${encoded_context}</li>
    <li> gateway = ${gateway}</li>
    <li> redirect_url = ${redirect_url}</li>
    <li> auth_url = ${auth_url}</li>
    <li> ccp_url = ${ccp_url}</li>
    <li> cdn_url = ${cdn_url}</li>
    <li> d4s_dynamic_scope = ${d4s_dynamic_scope}</li>
    <li> uma enabled= ${use_uma}</li>

</ul>

<h5> Environment</h5>

<ul>
    <li> current_context = ${current_context}</li>

    <li> current_url = ${current_url}</li>
    <li> infrastructure = ${infrastructure}</li>
    <li> absolute_current_url = ${absolute_current_url}</li>
    
    <li> base_url = ${base_url}</li>
    <li> group_url = ${group_url}</li>
    <li> group_name = ${group_name}</li>
    <li> host = ${host}</li>
</ul>
