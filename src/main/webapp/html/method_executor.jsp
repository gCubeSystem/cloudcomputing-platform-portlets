<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ include file="./init.jsp"%>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
<div>
    <d4s-ccp-executionform serviceurl="${ccp_url}">
        <script src="${cdn_url}/storage/d4s-storage.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/ol@v9.2.4/dist/ol.js"></script>
        <script src="${cdn_url}/ccp/js/inputwidgetcontroller.js"></script>
        <script src="${cdn_url}/ccp/js/outputwidgetcontroller.js"></script>
        <script src="${cdn_url}/ccp/js/executionformcontroller.js"></script>
        <script src="${cdn_url}/common/js/utils.js"></script>
    </d4s-ccp-executionform>
</div>
