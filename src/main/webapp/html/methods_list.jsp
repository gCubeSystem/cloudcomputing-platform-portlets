<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ include file="./init.jsp" %>

<div>
    <d4s-ccp-methodlist allow-execute="false" allow-edit="false" archive="true" serviceurl="${ccp_url}">
        <script src="${cdn_url}/ccp/js/methodlistcontroller.js"></script>
    </d4s-ccp-methodlist>
</div>

<script>

    function logButtonState() {
        var methodList = $('d4s-ccp-methodlist');

        var allowExecute = methodList.attr('allow-execute') === 'true';
        var allowEdit = methodList.attr('allow-edit') === 'true';
        var widgetExecute = $('d4s-ccp-executionform').length > 0 ? 'true' : 'false';
        var widgetEdit = $('d4s-ccp-methodeditor').length > 0 ? 'true' : 'false';

        console.log('allowExecute:', allowExecute, 'widgetExecute:', widgetExecute);
        console.log('allowEdit:', allowEdit, 'widgetEdit:', widgetEdit);
    }

    // Check for d4s-ccp-executionform and d4s-ccp-methodeditor, and abilitates the execute and edit buttons
    var updateButtonsLayout = function () {
        var methodListElement = document.querySelector('d4s-ccp-methodlist');

        if (methodListElement) {
            // Check if the element d4s-ccp-executionform exists in the DOM
            var allowExecute = document.querySelector('d4s-ccp-executionform') ? 'true' : 'false';
            methodListElement.setAttribute('allow-execute', allowExecute);

            // Check if the element d4s-ccp-methodeditor exists in the DOM
            var allowEdit = document.querySelector('d4s-ccp-methodeditor') ? 'true' : 'false';
            methodListElement.setAttribute('allow-edit', allowEdit);

            // Call the updateList() function of the controller if it exists
            if (typeof methodListElement.updateList === 'function') {
                methodListElement.connectedCallback(); // Reinitialize the component
                // methodListElement.updateList(); // Update the list
            }
        }
        //logButtonState(); // Log the current state
    }


    $(document).ready(function () {
        updateButtonsLayout();
    });
</script>