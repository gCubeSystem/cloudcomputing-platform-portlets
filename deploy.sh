#!/bin/bash

set -e

# Define server arrays for different environments
SERVERS_PRE=("lr62-pre-01" "lr62-pre-02")
SERVERS_PROD=("lr62-prod-01" "lr62-prod-02")
SERVERS_DEV=("lr62-dev")

# Set default deployment environment to 'dev', can override with 'pre' or 'prod'
ENVIRONMENT=${1:-dev}

# Select the appropriate server array based on the environment
case "$ENVIRONMENT" in
    pre)
        SERVERS=("${SERVERS_PRE[@]}")
        ;;
    prod)
        SERVERS=("${SERVERS_PROD[@]}")
        ;;
    *)
        SERVERS=("${SERVERS_DEV[@]}")
        ;;
esac

echo "Selected environment: $ENVIRONMENT"
echo "Deploying to servers: ${SERVERS[*]}"

# Retrieve Maven project name
MVN_NAME=$(mvn -q \
    -Dexec.executable=echo \
    -Dexec.args='${project.artifactId}' \
    --non-recursive \
    exec:exec)
echo "MVN_NAME=${MVN_NAME}"

# Retrieve Maven final name of the build artifact
MVN_FINALNAME=$(mvn -q \
    -Dexec.executable=echo \
    -Dexec.args='${project.build.finalName}' \
    --non-recursive \
    exec:exec)
echo "MVN_FINALNAME=${MVN_FINALNAME}"

# Execute Maven clean and package
mvn clean package

# Deploy to each server in the selected list
for HOST in "${SERVERS[@]}"; do
    scp target/$MVN_FINALNAME.war life@$HOST:/home/life/Portal-Bundle/deploy/$MVN_NAME.war
done

# Tail log from the last server in the list
ssh $HOST "sudo journalctl -fl -u liferay"
